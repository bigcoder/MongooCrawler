package com.trytech.mongoocrawler.client.transport;

/**
 * 连接服务器
 */
public class GenericConnector {

    //客户端
    private NettyClient client;

    /*****
     * 连接服务器
     */
    public void connect(String url, int port){
        client = NettyClient.newInstance(url,port);
    }
}
