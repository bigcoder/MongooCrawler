package com.trytech.mongoocrawler.client.transport;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by hp on 2017-1-24.
 */
public class TaskThreadPool {

    private ExecutorService threadPool = Executors.newCachedThreadPool();

    public void submit(TaskWorker worker){
        threadPool.submit(worker);
    }
}
