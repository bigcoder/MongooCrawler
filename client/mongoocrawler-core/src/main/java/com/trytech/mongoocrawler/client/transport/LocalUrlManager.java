package com.trytech.mongoocrawler.client.transport;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by coliza on 2017/4/9.
 */
public class LocalUrlManager extends UrlManager{
    public void pushUrl(String url){
        if(isUnique(url)){
            try {
                urlQueue.push(new URL(url));
            }catch (MalformedURLException e){
                //日志

            }
        }
    }
    public void pushUrls(String[] urls){
        for(String url:urls) {
            pushUrl(url);
        }
    }

    @Override
    public URL popUrl() {
        URL url = urlQueue.pop();
        return url;
    }

    /***
     * 获取url
     * @param timeout 单位：毫秒
     * @return
     */
    public URL fetchUrl(int timeout){
        try {
            return urlQueue.poll(timeout, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
