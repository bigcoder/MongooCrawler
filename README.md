#MongooCrawler
一款开源分布式爬虫软件，如果喜欢这个项目记得加star哈。
<br>
MongooCrawler为了解决抓取速度和持久化速度不一致的问题引入了disruptor并发框架
<br>
抓取的数据分为两类，一类是文本，一类是url，url暂时使用MD5进行去重，再放入队列文本则会直接进入数据库。
<br>
如果想私下交流可以在我blog留言
<br>
欢迎访问我的blog：http://blog.csdn.net/flashflight
#更新说明
2017-05-03 完成图书爬虫逻辑
<br>
2017-04-16 添加了电商图书类商品爬取逻辑
<br>
2017-01-12 添加了解析html的逻辑，依赖jsoup包
<br>
2017-04-12 添加了一个事例

#下一步计划
1、添加一个电商的商品详情解析器，尝试抓取电商数据并测试性能(已完成)
<br>
2、添加xml配置管理和注解功能
<br>
3、添加多爬虫实例支持（多session）