package com.trytech.mongoocrawler.client;

import com.trytech.mongoocrawler.client.xml.XmlConfigBean;
import com.trytech.mongoocrawler.client.xml.XmlDocumentBuilder;
import org.dom4j.DocumentException;
import org.xml.sax.SAXException;

import java.io.IOException;

/**
 * Created by hp on 2017-2-17.
 */
public class CrawlerConfig {
    private final static String CRAWLER_URL_FETCH_TIMEOUT = "crawler.url.fetch.timeout";
    private final static String CRAWLER_RUN_MODE = "crawler.run.mode";
    private final static String CRAWLER_START_URLS = "crawler.start_urls";
    private final static String CRAWLER_REDIS_IP = "crawler.redis.ip";
    private final static String CRAWLER_REDIS_PORT = "crawler.redis.port";
    private final static String CRAWLER_URL_STORE_MODE = "crawler.url_store_mode";
    private static XmlConfigBean configBean;

    public static enum URL_STORE_MODE{
        LOCAL("LOCAL"),REDIS("REDIS");
        private String value;
        URL_STORE_MODE(String value){
            this.value = value;
        }
        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
        public boolean equals(URL_STORE_MODE mode){
            return this.value.equals(mode.getValue());
        }
    }
    public static CrawlerConfig newInstance(String path) throws IOException, DocumentException, SAXException {
        XmlDocumentBuilder xmlDocumentBuilder = XmlDocumentBuilder.newInstance();
        configBean = xmlDocumentBuilder.parse(path);
        CrawlerConfig config = new CrawlerConfig();
        config.setConfigBean(configBean);
        return config;
    }

    /****************************
     * 获取运行模式
     ***************************/
    public Integer getRunMode(){
        return getInt(CRAWLER_RUN_MODE);
    }
    /****************************
     * 获取取url的超时时间
     ***************************/
    public Integer getFetchTimeout(){
        return getInt(CRAWLER_URL_FETCH_TIMEOUT);
    }
    /****************************
     * 获取起始url
     * @return
     ****************************/
    public String[] getStartUrls(){
        String start_url_str = getString(CRAWLER_START_URLS);
        if(start_url_str == null)return null;
        return start_url_str.split(";");
    }

    /**************************
     * 获取redis ip
     * @return
     **************************/
    public String getRedisIp(){
        return getString(CRAWLER_REDIS_IP);
    }
    /**************************
     * 获取redis端口
     * @return 端口
     **************************/
    public int getRedisPort(){
        return getInt(CRAWLER_REDIS_PORT);
    }
    /**********************************
     * url存储在哪里
     **********************************/
    public URL_STORE_MODE getUrlStoreMode(){
        if(URL_STORE_MODE.REDIS.equals(getString(CRAWLER_URL_STORE_MODE))){
            return URL_STORE_MODE.REDIS;
        }
        return URL_STORE_MODE.LOCAL;
    }
    private String getString(String key){

        return null;
    }
    private Integer getInt(String key){

        return null;
    }

    public static XmlConfigBean getConfigBean() {
        return configBean;
    }

    public static void setConfigBean(XmlConfigBean configBean) {
        CrawlerConfig.configBean = configBean;
    }
}
