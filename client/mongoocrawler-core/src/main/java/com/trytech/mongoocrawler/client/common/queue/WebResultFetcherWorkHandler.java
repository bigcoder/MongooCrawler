package com.trytech.mongoocrawler.client.common.queue;

import com.lmax.disruptor.WorkHandler;
import com.trytech.mongoocrawler.client.common.http.WebResult;
import com.trytech.mongoocrawler.client.common.util.DateUtils;
import com.trytech.mongoocrawler.client.common.util.MySQLUtils;
import com.trytech.mongoocrawler.client.entity.JDItem;
import com.trytech.mongoocrawler.client.parser.HtmlParser;
import com.trytech.mongoocrawler.client.parser.jd.JDBookCate1Parser;
import com.trytech.mongoocrawler.client.parser.jd.JDBookCate2Parser;
import com.trytech.mongoocrawler.client.parser.jd.JDBookDetailParser;
import com.trytech.mongoocrawler.client.parser.jd.JDBookListParser;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;

/**
 * Created by hp on 2017-1-25.
 */
public class WebResultFetcherWorkHandler extends FetcherWorkHandler implements WorkHandler<WebResultFetcherEvent> {

    private UrlFetcherEventProducer urlFetcherEventProducer;

    public WebResultFetcherWorkHandler(DisruptorContext disruptorContext){
        super(disruptorContext);
        this.urlFetcherEventProducer =  new UrlFetcherEventProducer(disruptorContext.getUrlResultRingBuffer());
    }

    @Override
    public void onEvent(WebResultFetcherEvent webResultFetcherEvent) throws Exception {
        WebResult<String> webResult = webResultFetcherEvent.getData();
        if(webResult != null) {
            String html = ((WebResult<String>)webResult).getData();
            if(StringUtils.isNotEmpty(html)){
                try {
//                    List<LianjiaItem> itemList = LianjiaHtmlParser.parse(html);
//                    for(LianjiaItem item : itemList){
//                        String sql = "insert into lj_house(`C_TITLE`,`C_LOCATION`,`C_TYPE`,`C_FLOORSPACE`,`C_PRICE`,`C_UNIT_PRICE`) values('" + item.getTitle() + "','" + item.getLocation() + "','"+item.getType()+
//                                "','"+item.getFloorSpace()+"',"+item.getPrice().toString()+","+item.getUnitPrice().toString()+")";
//                        MySQLUtils.insert(sql);
//                    }
                    HtmlParser parser = webResult.getParser();
                    if(parser instanceof JDBookCate1Parser){
                        Boolean isSuccess  = ((JDBookCate1Parser)webResult.getParser()).parse(webResult,this.urlFetcherEventProducer);
                    }else if(parser instanceof JDBookCate2Parser){
                        Boolean isSuccess  = ((JDBookCate2Parser)webResult.getParser()).parse(webResult,this.urlFetcherEventProducer);
                    }else if(parser instanceof JDBookListParser){
                        Boolean isSuccess  = ((JDBookListParser)webResult.getParser()).parse(webResult,this.urlFetcherEventProducer);
                    }else if(parser instanceof JDBookDetailParser){
                        JDItem item  = ((JDBookDetailParser)webResult.getParser()).parse(webResult,this.urlFetcherEventProducer);
                        if(item != null) {
                            String sql = "insert into jd(`NAME`,`PRICE`,`AUTHOR`,`LANGUAGE`,`AGENT`,`ISBN`,`NO`,`URL`,`CREATE_TIME`) values(\"" + item.getName() + "\"," + item.getPrice().toString() + ",\"" + item.getAuthor() +
                                    "\",\"" + item.getLanguage() + "\",\"" + item.getAgent() + "\",\"" + item.getIsbn() + "\",\"" + item.getNo() + "\",\"" + item.getUrl() + "\",\""+ DateUtils.now("yyyy-MM-dd HH:mm:ss")+"\")";
                            System.out.println(sql);
                            MySQLUtils.insert(sql);
                        }
                    }
                }catch (SQLException ex){
                    ex.printStackTrace();
                }
            }
        }
    }
}
