package com.trytech.mongoocrawler.client.common.http;

import com.alibaba.fastjson.JSONObject;
import com.trytech.mongoocrawler.client.parser.HtmlParser;

import java.io.Serializable;

/**
 * the <code>WebResult</code> class is generic type class,
 * which is used to represent the content from internet
 * @author jeff chiang
 * @date 2016-11-03
 * @since V1.0
 */
public class WebResult<T extends Serializable> {
    private T data;
    private StatusCode statuscode;
    private String message;
    private String url;
    private HtmlParser parser;

    public static enum StatusCode{
        SUCCESS(1),FAILURE(0);
        private int statuscode;
        StatusCode(int statuscode){
            this.statuscode = statuscode;
        }
        public int getStatuscode(){
            return statuscode;
        }
    }

    public  WebResult(StatusCode statuscode, T data, String message)
    {
        this.statuscode = statuscode;
        this.data = data;
        this.message = message;
    }

    public HtmlParser getParser() {
        return parser;
    }

    public void setParser(HtmlParser parser) {
        this.parser = parser;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public StatusCode getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(StatusCode statuscode) {
        this.statuscode = statuscode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public JSONObject getJSON(){
        JSONObject result = new JSONObject();
        result.put("statuscode",statuscode);
        result.put("message", message);
        result.put("data", data);
        System.out.println(result.toJSONString());
        return result;
    }
    public static <T extends Serializable> WebResult<T> newSuccessResult(T data){
        return newSuccessResult(StatusCode.SUCCESS,data);
    }
    public static <T extends Serializable> WebResult<T> newSuccessResult(StatusCode statuscode, T data){
        return new WebResult<T>(statuscode,data,null);
    }
    public static <T extends Serializable> WebResult<T> newSuccessResult(StatusCode statuscode, T data, String msg){
        return new WebResult<T>(statuscode,data,msg);
    }
}
