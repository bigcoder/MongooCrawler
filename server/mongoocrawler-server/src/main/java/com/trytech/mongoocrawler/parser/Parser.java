package com.trytech.mongoocrawler.parser;

/**
 * <code>Parser</code>定义了解析器规范
 * @author coliza@sina.com
 * @date 2016年11月25日
 */
public interface Parser<T> {

    /*****************
     * 解析页面
     *****************/
    public T parse(String html);
}
