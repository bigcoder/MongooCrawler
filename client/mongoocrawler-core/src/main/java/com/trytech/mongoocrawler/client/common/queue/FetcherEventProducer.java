package com.trytech.mongoocrawler.client.common.queue;

import com.lmax.disruptor.EventTranslatorOneArg;
import com.lmax.disruptor.RingBuffer;

/**
 * fetcher事件生产者
 */
public abstract class FetcherEventProducer<A extends FetcherEvent,T> {
    protected final EventTranslatorOneArg<A, T> TRANSLATOR =
            new EventTranslatorOneArg<A, T>() {
                public void translateTo(A event, long sequence, T data) {
                    event.setData(data);
                }
            };
    protected RingBuffer<A> ringBuffer;
    public FetcherEventProducer(RingBuffer<A> ringBuffer) {
        this.ringBuffer = ringBuffer;
    }

    protected FetcherEventProducer() {
    }
    public void sendData(T data) {
        ringBuffer.publishEvent(TRANSLATOR,data);
    }

}
