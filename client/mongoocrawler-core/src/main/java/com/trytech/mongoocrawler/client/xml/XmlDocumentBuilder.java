package com.trytech.mongoocrawler.client.xml;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;

/**
 * Xml配置对象构造器
 * 使用dom4j来解析xml
 */
public class XmlDocumentBuilder {
    //xml解析器
    private SAXReader saxParser;

    public static XmlDocumentBuilder newInstance(){
        XmlDocumentBuilder builder = new XmlDocumentBuilder();
        return builder;
    }

    public XmlConfigBean parse(String path) throws SAXException, IOException, DocumentException {
        if(saxParser == null) {
            saxParser = new SAXReader();
        }
        Document doc = saxParser.read(new File(path));
        XmlConfigBean xmlConfigBean = new XmlConfigBean();
        return xmlConfigBean;
    }


}
