package com.trytech.mongoocrawler.client;

/**
 * 爬虫启动类
 */
public class CrawlerStart {
    public static void main(String args[]) {
        String url = " https://book.jd.com/";
        CrawlerContext crawlerContext = new CrawlerContext();
        CrawlerSession crawlerSession = new CrawlerSession(crawlerContext, new String[]{url});
        crawlerContext.registerSession(crawlerSession);
        crawlerContext.start();
    }
}
