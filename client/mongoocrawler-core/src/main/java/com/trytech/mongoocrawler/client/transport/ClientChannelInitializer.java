package com.trytech.mongoocrawler.client.transport;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;


/**
 * Created by coliza on 2016/11/11.
 */
public class ClientChannelInitializer<NioSocketChannel extends Channel> extends ChannelInitializer<NioSocketChannel> {

    @Override
    public void initChannel(NioSocketChannel ch) throws Exception {
        ch.pipeline().addLast(new ClientEventHandler());
    }
}
