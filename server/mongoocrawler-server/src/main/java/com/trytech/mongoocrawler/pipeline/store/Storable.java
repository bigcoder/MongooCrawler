package com.trytech.mongoocrawler.pipeline.store;

/**
 * 存储接口，规范统一存储调用接口
 * @author coliza@sina.com
 * @date 2016年11月25日
 */
public interface Storable {
    /**************
     * 存储对象
     **************/
    public void store() throws Exception;
}
