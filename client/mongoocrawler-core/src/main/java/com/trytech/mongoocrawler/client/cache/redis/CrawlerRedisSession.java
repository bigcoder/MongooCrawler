package com.trytech.mongoocrawler.client.cache.redis;

import com.trytech.mongoocrawler.client.CrawlerContext;
import com.trytech.mongoocrawler.client.CrawlerSession;
import com.trytech.mongoocrawler.client.transport.RedisUrlManager;

/**
 * Created by coliza on 2017/4/9.
 */
public class CrawlerRedisSession extends CrawlerSession
{
    private RedisClient redisClient;
    private String queue_key;
    /***
     * 构造函数

     * @param container 父容器
     * @param client
     */
    public CrawlerRedisSession(CrawlerContext container, RedisClient client, String queue_key) {
        super(container, null);
        this.redisClient = client;
        this.queue_key = queue_key;
        String[] startsUrls = new String[]{client.fetchFromQueue(queue_key)};
        this.urlManager = new RedisUrlManager(this,redisClient);
        this.urlManager.pushUrls(startsUrls);
    }

    public String getRedisKey(){
        return queue_key;
    }
}
