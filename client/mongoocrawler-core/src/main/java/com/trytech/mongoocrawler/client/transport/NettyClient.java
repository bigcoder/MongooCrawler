package com.trytech.mongoocrawler.client.transport;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * Created by coliza on 2016/11/11.
 */
public class NettyClient {
    private String host;
    private int port;

    public static NettyClient newInstance(String host,int port){
        NettyClient nettyClient = new NettyClient(host,port);
        return nettyClient;
    }

    public NettyClient(String host,int port){
        init();
    }
    public void init(){
        //创建工作线程池
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            //创建启动器
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group).channel(NioSocketChannel.class).option(ChannelOption.TCP_NODELAY,true)
                     .handler(new ClientChannelInitializer());
            ChannelFuture channelFuture = bootstrap.connect();
            channelFuture.channel().closeFuture().sync();
        }catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            group.shutdown();
        }
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
