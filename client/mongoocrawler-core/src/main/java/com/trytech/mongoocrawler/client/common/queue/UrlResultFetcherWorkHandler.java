package com.trytech.mongoocrawler.client.common.queue;

import com.lmax.disruptor.WorkHandler;
import com.trytech.mongoocrawler.client.CrawlerSession;
import com.trytech.mongoocrawler.client.common.http.CrawlerHttpRequest;
import com.trytech.mongoocrawler.client.common.http.UrlResult;
import com.trytech.mongoocrawler.client.common.http.factory.CrawlerHttpReqFactory;

import java.net.URL;

/**
 * Created by coliza on 2017/4/8.
 */
public class UrlResultFetcherWorkHandler extends FetcherWorkHandler implements WorkHandler<UrlResultFetcherEvent> {

    public UrlResultFetcherWorkHandler(DisruptorContext disruptorContext){
        super(disruptorContext);
    }

    @Override
    public void onEvent(UrlResultFetcherEvent event) throws Exception {
        CrawlerSession session = disruptorContext.getCrawlerSession();
        UrlResult urlResult = event.getData();
        CrawlerHttpRequest request = CrawlerHttpReqFactory.getRequest(new URL(urlResult.getUrl()),urlResult.getParser());
        session.submitTask(request);
    }
}
