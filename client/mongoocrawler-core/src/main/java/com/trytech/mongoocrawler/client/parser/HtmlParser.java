package com.trytech.mongoocrawler.client.parser;

import com.trytech.mongoocrawler.client.common.http.WebResult;
import com.trytech.mongoocrawler.client.common.queue.UrlFetcherEventProducer;
import com.trytech.mongoocrawler.client.entity.JDItem;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by coliza on 2017/4/10.
 */
public abstract class HtmlParser<T> {
    public abstract T parse(WebResult webResult, UrlFetcherEventProducer urlProducer);
    protected Element getBody(String html){
        List<JDItem> itemList = new LinkedList<JDItem>();
        Document doc = Jsoup.parse(html);
        doc.charset(Charset.forName("UTF-8"));
        Element body = doc.body();
        return body;
    }
}
